export function generatePathForUpload(modelName: string, file: Express.Multer.File) {
    let path = `${modelName}`
    if (file.fieldname.includes('textures')) {
        path = `${modelName}/textures/${file.originalname}`
    } else {
        path = `${modelName}/${file.originalname}`
    }
    return path
}

export function BufferToBlob(buffer: Buffer, contentType = '') {
    const b64Data = Buffer.from(buffer).toString('base64')

    const byteCharacters = atob(b64Data)

    const byteNumbers = new Array(byteCharacters.length)
    for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i)
    }

    const byteArray = new Uint8Array(byteNumbers)

    const blob = new Blob([byteArray], { type: contentType })
    return blob
}

export const binArrayToJson = function (binArray: any) {
    let str = ''
    for (let i = 0; i < binArray.length; i++) {
        str += String.fromCharCode(parseInt(binArray[i]))
    }
    return JSON.parse(str)
}

export function toArrayBuffer(buffer: Buffer) {
    const arrayBuffer = new ArrayBuffer(buffer.length)
    const view = new Uint8Array(arrayBuffer)
    for (let i = 0; i < buffer.length; ++i) {
        view[i] = buffer[i]
    }
    return arrayBuffer
}

export const blobToFile = (theBlob: Blob, fileName: string): File => {
    const b: any = theBlob
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date()
    b.name = fileName

    //Cast to a File() type
    return theBlob as File
}
