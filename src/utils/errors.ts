import { Result, ValidationError } from 'express-validator'
import { IResponseError } from '~/type'

export function ErrorResponse(
    serverMessage: string,
    status: number = 500,
    errors?: Result<ValidationError>,
    errorMessage?: string
) {
    const error: IResponseError = new Error(serverMessage)
    error.status = status
    error.message = errorMessage ? errorMessage : serverMessage
    if (!errors?.isEmpty()) {
        error.messages = {}
        const errorObj = errors?.mapped()
        for (const key in errorObj) {
            error.messages![key] = errorObj[key].msg
        }
    }
    return error
}
