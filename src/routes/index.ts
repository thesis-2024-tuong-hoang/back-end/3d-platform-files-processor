/* eslint-disable @typescript-eslint/no-var-requires */
import { Router } from 'express'
import multer from 'multer'
import FileController from '~/controllers/fileController'

const whitelist = [
    'image/png',
    'image/jpeg',
    'image/jpg',
    'image/webp',
    'application/octet-stream',
    'model/gltf+json',
    'text/plain'
]

// async function createZipArchive() {
//     const zip = new AdmZip()
//     const outputFile = 'test.zip'
//     zip.addLocalFolder('./test')
//     zip.writeZip(outputFile)
//     console.log(`Created ${outputFile} successfully`)
// }

const router = Router()
const upload = multer({
    fileFilter: (req, file, cb) => {
        if (!whitelist.includes(file.mimetype)) {
            return cb(new Error('file is not allowed'))
        }
        cb(null, true)
    }
})

const cpUpload = upload.any()

router.post('/upload/:model', cpUpload, FileController.upload)

router.get('/download/:model', cpUpload, FileController.download)

export default router
