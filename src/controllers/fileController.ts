import * as express from 'express'
// import multer from 'multer'
import { ErrorResponse } from '~/utils/errors'
import { uploadFile } from '~/constants/supabaseConfig'
import { BufferToBlob, generatePathForUpload, binArrayToJson } from '~/utils/file'
import supabase from '~/constants/supabaseConfig'

class FileController {
    static async upload(req: express.Request, res: express.Response, next: express.NextFunction) {
        const gltfFiles = req.files as Express.Multer.File[]

        try {
            gltfFiles.forEach(async (file) => {
                const newBlob = BufferToBlob(file.buffer, file.mimetype)
                const path = generatePathForUpload(req.params.model, file)
                const response = await uploadFile('3D-models', path, newBlob, file.mimetype)
                if (!response) {
                    const newError = ErrorResponse(
                        `Cannot save file ${file.originalname} on database`,
                        500,
                        undefined,
                        undefined
                    )
                    throw newError
                }
            })
            res.status(200).json({ message: 'Save new model successfully !' })
        } catch (error) {
            next(error)
        }
    }

    // static async upload(req: express.Request, res: express.Response, next: express.NextFunction) {
    //     console.log(req)

    //     try {
    //         gltfFiles.forEach(async (file) => {
    //             const newBlob = BufferToBlob(file.buffer, file.mimetype)
    //             const path = generatePathForUpload(req.params.model, file)
    //             const response = await uploadFile('3D-models', path, newBlob, file.mimetype)
    //             if (!response) {
    //                 const newError = ErrorResponse(
    //                     `Cannot save file ${file.originalname} on database`,
    //                     500,
    //                     undefined,
    //                     undefined
    //                 )
    //                 throw newError
    //             }
    //         })
    //         res.status(200).json({ message: 'Save new model successfully !' })
    //     } catch (error) {
    //         next(error)
    //     }
    // }

    static async download(req: express.Request, res: express.Response, next: express.NextFunction) {
        const modelName = req.params.model
        try {
            const { data: blob } = await supabase.storage.from('3D-models').download(`${modelName}/scene.gltf`)
            if (blob) {
                const u8arrayFile = new Uint8Array(await blob.arrayBuffer())
                const jsonFile = binArrayToJson(u8arrayFile)
                const fileData = {
                    title: jsonFile.asset.extras.title,
                    textures: jsonFile.images,
                    buffers: jsonFile.buffers
                }
                res.status(200).json(fileData)
            }
        } catch (error) {
            next(error)
        }
    }
}

export default FileController
