import express from 'express'
import logger from 'morgan'
import router from './routes'
import multer from 'multer'
import cookieParser from 'cookie-parser'
import errorHandler from './middlewares/error'
import cors from 'cors'

const app = express()

const corsOptions = {
    origin: '*',
    methods: ['GET, POST, PUT, DELETE, PATCH'],
    allowedHeaders: ['Content-Type']
}

app.use(cors(corsOptions))

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.use('/process-file', router)

app.use(errorHandler)

console.log('3D File Processor running on localhost:3005')

app.listen(3005)
