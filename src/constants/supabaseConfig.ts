import 'dotenv/config'
import { createClient } from '@supabase/supabase-js'
import { Database } from './database.types'
import { SUPABASE_URL, SUPABASE_ANON_KEY } from './constants'

const supabase = createClient<Database>(SUPABASE_URL, SUPABASE_ANON_KEY, {
    auth: { persistSession: false }
})

export async function uploadFile(bucketName: string, fileName: string, file: any, type: string) {
    const { data, error } = await supabase.storage.from(bucketName).upload(fileName, file, {
        contentType: type,
        cacheControl: '3600',
        upsert: false
    })
    if (error) {
        // Handle error
        console.log(error)
    } else {
        return data
    }
}

export default supabase
