export type Json = string | number | boolean | null | { [key: string]: Json | undefined } | Json[]

export interface Database {
    public: {
        Tables: {
            credentials: {
                Row: {
                    created_at: string
                    email: string
                    id: number
                    password: string
                    user_id: string
                }
                Insert: {
                    created_at?: string
                    email: string
                    id?: number
                    password: string
                    user_id?: string
                }
                Update: {
                    created_at?: string
                    email?: string
                    id?: number
                    password?: string
                    user_id?: string
                }
                Relationships: [
                    {
                        foreignKeyName: 'credentials_user_id_fkey'
                        columns: ['user_id']
                        isOneToOne: false
                        referencedRelation: 'users'
                        referencedColumns: ['id']
                    }
                ]
            }
            'model-tag': {
                Row: {
                    created_at: string
                    id: number
                    model_id: string
                    tag_id: number
                }
                Insert: {
                    created_at?: string
                    id?: number
                    model_id: string
                    tag_id: number
                }
                Update: {
                    created_at?: string
                    id?: number
                    model_id?: string
                    tag_id?: number
                }
                Relationships: [
                    {
                        foreignKeyName: 'model-tag_model_id_fkey'
                        columns: ['model_id']
                        isOneToOne: false
                        referencedRelation: 'models'
                        referencedColumns: ['id']
                    },
                    {
                        foreignKeyName: 'model-tag_tag_id_fkey'
                        columns: ['tag_id']
                        isOneToOne: false
                        referencedRelation: 'tags'
                        referencedColumns: ['id']
                    }
                ]
            }
            models: {
                Row: {
                    created_at: string
                    description: string | null
                    downloaded: number
                    id: string
                    liked: string[] | null
                    metadata: string
                    stars: number
                    user_id: string
                    viewed: number
                }
                Insert: {
                    created_at?: string
                    description?: string | null
                    downloaded?: number
                    id?: string
                    liked?: string[] | null
                    metadata: string
                    stars: number
                    user_id: string
                    viewed?: number
                }
                Update: {
                    created_at?: string
                    description?: string | null
                    downloaded?: number
                    id?: string
                    liked?: string[] | null
                    metadata?: string
                    stars?: number
                    user_id?: string
                    viewed?: number
                }
                Relationships: [
                    {
                        foreignKeyName: 'models_user_id_fkey'
                        columns: ['user_id']
                        isOneToOne: false
                        referencedRelation: 'users'
                        referencedColumns: ['id']
                    }
                ]
            }
            tags: {
                Row: {
                    created_at: string
                    id: number
                    name: string
                }
                Insert: {
                    created_at?: string
                    id?: number
                    name: string
                }
                Update: {
                    created_at?: string
                    id?: number
                    name?: string
                }
                Relationships: []
            }
            tokens: {
                Row: {
                    created_at: string
                    id: number
                    token: string
                    user_id: string | null
                }
                Insert: {
                    created_at?: string
                    id?: number
                    token: string
                    user_id?: string | null
                }
                Update: {
                    created_at?: string
                    id?: number
                    token?: string
                    user_id?: string | null
                }
                Relationships: [
                    {
                        foreignKeyName: 'tokens_user_id_fkey'
                        columns: ['user_id']
                        isOneToOne: false
                        referencedRelation: 'users'
                        referencedColumns: ['id']
                    }
                ]
            }
            users: {
                Row: {
                    avatar: string | null
                    city: string | null
                    country: string | null
                    created_at: string
                    email: string | null
                    firstname: string | null
                    followers: string[] | null
                    following: string[] | null
                    id: string
                    lastname: string | null
                    'profession ': string | null
                    skills: string[] | null
                }
                Insert: {
                    avatar?: string | null
                    city?: string | null
                    country?: string | null
                    created_at?: string
                    email?: string | null
                    firstname?: string | null
                    followers?: string[] | null
                    following?: string[] | null
                    id?: string
                    lastname?: string | null
                    'profession '?: string | null
                    skills?: string[] | null
                }
                Update: {
                    avatar?: string | null
                    city?: string | null
                    country?: string | null
                    created_at?: string
                    email?: string | null
                    firstname?: string | null
                    followers?: string[] | null
                    following?: string[] | null
                    id?: string
                    lastname?: string | null
                    'profession '?: string | null
                    skills?: string[] | null
                }
                Relationships: []
            }
        }
        Views: {
            [_ in never]: never
        }
        Functions: {
            [_ in never]: never
        }
        Enums: {
            [_ in never]: never
        }
        CompositeTypes: {
            [_ in never]: never
        }
    }
}
