export interface IGltfFileUpload {
    fieldname: string
    originalname: string
    encoding: string
    mimetype: string
    destination: string
    filename: string
    path: string
    size: number
}

export interface IResponseError extends Error {
    status?: number
    messages?: { [errorName: string]: string }
}
